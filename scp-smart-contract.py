import smartpy as sp

class NozamaSCP(sp.Contract):
    def __init__(self, admin):
        self.init(administrator = admin, scps = sp.big_map(tkey = sp.TAddress))

    @sp.entry_point
    def createSCP(self, params):
        self.addSCPIfNecessary(params)

    def addSCPIfNecessary(self, params):
        sp.verify(sp.sender == self.data.administrator)
        sp.if ~ self.data.scps.contains(params.address):
            self.data.scps[params.address] = sp.record(co2 = 0, total_recycled = 0, recycled = sp.map(tkey = sp.TString, tvalue = sp.TNat),  pickups = sp.map(tkey = sp.TString))

    def addPickupIfNecessary(self, params):
        sp.verify(sp.sender == self.data.administrator)
        sp.if ~ self.data.scps[params.address].pickups.contains(params.pickup_id):
            self.data.scps[params.address].pickups[params.pickup_id] = sp.record(checkpoints = sp.map(tkey = sp.TString, tvalue = sp.TRecord(agent_id = sp.TString, lat = sp.TString, lon = sp.TString, ptype = sp.TString, time = sp.TTimestamp)), total_recycled = 0, recycled = {})

    @sp.entry_point
    def addCheckpoint(self, params):
        self.addSCPIfNecessary(params)
        self.addPickupIfNecessary(params)
        self.data.scps[params.address].pickups[params.pickup_id].checkpoints[params.ptype] = sp.record(ptype = params.ptype, lat = params.lat, lon = params.lon, agent_id = params.agent_id, time = sp.now)

    @sp.entry_point
    def addRecycle(self, params):
        self.addSCPIfNecessary(params)
        self.data.scps[params.address].pickups[params.pickup_id].recycled[params.material] = params.gr
        sp.if self.data.scps[params.address].recycled.contains(params.material):
            self.data.scps[params.address].recycled[params.material] += params.gr
        sp.else:
            self.data.scps[params.address].recycled[params.material] = params.gr



@sp.add_test(name = "Contract")
def test():
    creator = sp.test_account("Creator")
    user1 = sp.test_account("User1")
    user2 = sp.test_account("User2")
    c1 = NozamaSCP(creator.address)
    scenario = sp.test_scenario()
    scenario += c1
    scenario += c1.createSCP(address = user1.address).run(sender = creator)
    scenario += c1.addCheckpoint(address = user1.address, pickup_id = "p123", ptype = "Recovery", lat = "1.4543534543", lon = "14.4535345", agent_id = "Jona_1").run(sender = creator)
    scenario += c1.addRecycle(address = user1.address, pickup_id = "p123", material = "alu", gr = 20).run(sender = creator)
    scenario += c1.addRecycle(address = user1.address, pickup_id = "p123", material = "pet1", gr = 80).run(sender = creator)
    scenario += c1.addCheckpoint(address = user1.address, pickup_id = "p124", ptype = "Recovery", lat = "1.4543534543", lon = "14.4535345", agent_id = "Carlos_1").run(sender = creator)
    scenario += c1.addRecycle(address = user1.address, pickup_id = "p124", material = "pet1", gr = 80).run(sender = creator)
    scenario += c1.addRecycle(address = user1.address, pickup_id = "p124", material = "alu", gr = 10).run(sender = creator)

    scenario += c1.createSCP(address = user2.address).run(sender = creator)
    scenario += c1.addCheckpoint(address = user2.address, pickup_id = "p125", ptype = "Recovery", lat = "1.4543534543", lon = "14.4535345", agent_id = "Carlos_1").run(sender = creator)
    scenario += c1.addRecycle(address = user2.address, pickup_id = "p125", material = "hdp2", gr = 30).run(sender = creator)
